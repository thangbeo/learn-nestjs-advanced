import { Controller, Get, Query } from '@nestjs/common';
import { FibonacciWorkerHost } from './fibonacci-worker.host';
import Piscina from 'piscina';
import { resolve } from 'path';

@Controller('fibonacci')
export class FibonacciController {
  fibonacciWorker = new Piscina({
    filename: resolve(__dirname, 'fibonacci-piscina.worker.js'),
  });

  constructor(private readonly fibonacciWorkerHost: FibonacciWorkerHost) {}

  @Get()
  fibonacci(@Query('n') n = 10) {
    // if (n < 2) {
    //   return n;
    // }
    // return this.fibonacci(n - 1) + this.fibonacci(n - 2);
    // return this.fibonacciWorkerHost.run(n);
    return this.fibonacciWorker.run(n);
  }
}
